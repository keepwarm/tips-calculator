
//calculate tip
function calculate(){
    var prices = document.getElementById('prices').value;
    var tip = document.getElementById('tip').value;
    var other = document.getElementById('other').value;

    //calculator tip
    var total = 0;
    var finalTotal = 0;
    
    //validate input
    if (prices === '' || tip === 0){
        alert ('Please enter values');
        return;
    }
    if (tip === 'other'){
        total += (prices * other)/100;
    }
    else {
        total += prices * tip;
    }

    //round to two decimal places
    total = Math.round(total*100)/100;
    finalTotal = Number(total) + Number(prices);
 
    //display the tip
    document.getElementById('tipAmount').style.display = 'block';
    document.getElementById('totalTips').innerHTML = total;

    document.getElementById('totalAmount').style.display = 'block';
    document.getElementById('amountTotal').innerHTML = finalTotal;
};

    //hide the tip amount on load
    document.getElementById('tipAmount').style.display = 'none';
    document.getElementById('totalAmount').style.display = 'none';

    //click to call function
    document.getElementById("amount").onclick = function() {
        calculate();
      
      };